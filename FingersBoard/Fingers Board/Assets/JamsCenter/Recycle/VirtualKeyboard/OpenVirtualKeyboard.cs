﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenVirtualKeyboard : MonoBehaviour {

    [System.Serializable]
    public class  IndexType {
        public TouchScreenKeyboardType m_type;
    }

    public IndexType[] m_index;



    public static void OpenDefault() { Open(TouchScreenKeyboardType.Default); }
    public static void OpenNumPad() { Open(TouchScreenKeyboardType.NumberPad); }
    public static void OpenEmail() { Open(TouchScreenKeyboardType.EmailAddress); }
    public static void OpenASCII() { Open(TouchScreenKeyboardType.ASCIICapable); }
    public static void OpenPhone() { Open(TouchScreenKeyboardType.PhonePad); }
    public static void OpenNamePhone() { Open(TouchScreenKeyboardType.NamePhonePad); }
    public static void OpenSearch() { Open(TouchScreenKeyboardType.Search); }
    public static void OpenSocial() { Open(TouchScreenKeyboardType.Social); }
    public static void OpenURL() { Open(TouchScreenKeyboardType.URL); }

    public static void Open(TouchScreenKeyboardType type) {

        TouchScreenKeyboard.Open("", type,true,true,false);

    }

    public void Open(int scrollIndex) {
        if(scrollIndex>=0 && scrollIndex< m_index.Length)
            Open(m_index[scrollIndex].m_type);
    }
}
