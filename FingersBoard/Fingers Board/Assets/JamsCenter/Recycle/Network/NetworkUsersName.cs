﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkUsersName : NetworkBehaviour {

    [SyncVar]
    public string m_userName;

	// Use this for initialization
	void Start () {
        ChangeUserName(SystemInfo.deviceName);
    }

	void ChangeUserName (string name) {
        if(isLocalPlayer && hasAuthority)
            CmdChangeUserName(name);

    }

    [Command]
    public void CmdChangeUserName(string name) {
        m_userName = name;
    }

    internal static string GetMine()
    {
        return FindObjectsOfType<NetworkUsersName>().Where(t => t.isLocalPlayer && t.hasAuthority).First().m_userName; 
    }

    internal static List<string> GetUserNames()
    {
       return  FindObjectsOfType<NetworkUsersName>().Select(t => t.m_userName).ToList();
    }

    internal static string[] GetOthers()
    {

            List<string> names = GetUserNames();
            names.Remove(GetMine());
        return names.ToArray();
        
    }
}
