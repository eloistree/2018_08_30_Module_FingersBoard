﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ListenToUnityKeyCode : MonoBehaviour {

    public List<KeyCode> m_unityKeyCodeListened;
    public List<KeyCode> m_pressed = new List<KeyCode>();

    [System.Serializable]
    public class OnKeyCodeEvent : UnityEvent<KeyCode> { }
    public OnKeyCodeEvent m_onKeyCodeDown;
    public OnKeyCodeEvent m_onKeyCodeUp;

    public Dictionary<KeyCode, bool> m_keysState = new Dictionary<KeyCode, bool>();
    // Use this for initialization
    void Update () {
        
        KeyCode kc;
        for (int i = 0; i < m_unityKeyCodeListened.Count; i++)
        {
            kc = m_unityKeyCodeListened[i];
            bool keyState = Input.GetKey(kc);
            if (keyState != GetPreviousState(kc))
            {
                SetNewStateTo(kc, keyState);
                if (keyState)
                {
                    m_pressed.Add(kc);
                    if (m_onKeyCodeDown != null)
                        m_onKeyCodeDown.Invoke(kc);
                }
                if (!keyState)
                {
                    m_pressed.Remove(kc);
                    if (m_onKeyCodeUp != null)
                        m_onKeyCodeUp.Invoke(kc);
                }
            }


        }
	}

    private void SetNewStateTo(KeyCode kc, bool keyState)
    {
        CheckKeystateIsIn(kc);
        m_keysState[kc] = keyState;
    }
    private bool GetPreviousState(KeyCode kc)
    {
        CheckKeystateIsIn(kc);
        return m_keysState[kc];
    }

    private void CheckKeystateIsIn(KeyCode kc)
    {
        if (!m_keysState.ContainsKey(kc))
            m_keysState.Add(kc, false);
    }


    void Reset()
    {

        m_unityKeyCodeListened = FingersBoard.GetEnumList<KeyCode>();
    }
}
