﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyCodeListener : MonoBehaviour {


    public KeyCode m_key;
    public UnityEvent m_down;
    public UnityEvent m_up;

	void Update ()
    {
        if (m_key != KeyCode.None) {
            if (Input.GetKeyDown(m_key)) {

             //   Debug.Log("KC:" + m_key);
                m_down.Invoke();
            }
            if (Input.GetKeyUp(m_key)) m_up.Invoke();

        }
        //foreach (KeyCode kc in MacroUtility.GetEnumList<KeyCode>())
        //{
        //    if (Input.GetKeyDown(kc))
        //}
    }
}
