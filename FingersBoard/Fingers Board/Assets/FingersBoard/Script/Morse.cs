﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MorseValue
{
    public static char SHORT = '.';
    public static char LONG = '-';
    public static char SPACE = ' ';
    public MorseValue(params MorseKey[] keys) {
        m_value = keys;
    }
    [SerializeField]
    private MorseKey[] m_value;

    public override string ToString()
    {
        string value = "";
        for (int i = 0; i < m_value.Length; i++)
        {
            switch (m_value[i])
            {
                case MorseKey.Short:
                    value += SHORT;
                    break;
                case MorseKey.Long:
                    value += LONG;
                    break;
                default:
                    value += SPACE;
                    break;
            }
        }
        return value;
    }

    public static bool operator ==(MorseValue b1, MorseValue b2)
    {
        return b1.Equals(b2);
    }

    public static bool operator !=(MorseValue b1, MorseValue b2)
    {
        return !(b1 == b2);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
            return false;

        var b2 = (MorseValue)obj;

        if (m_value.Length != b2.m_value.Length)
            return false;
        for (int i = 0; i < m_value.Length; i++)
        {
            if (m_value[i] != b2.m_value[i])
                return false;
        }
        return true;
    }

    public override int GetHashCode()
    {
        return ToString().GetHashCode() ;
    }


}
[System.Serializable]
public class MorsePlus
{
    public MorsePlus( params MorseKey[] keys)
    {
        SetValue("", keys);
    }
    public MorsePlus(string description, params MorseKey[] keys)
    {
        SetValue(description, keys);

    }
    private void SetValue(string description, MorseKey[] keys) {
        m_morsValue = new MorseValue(keys);
        m_description = description;
    }

    [SerializeField]
    public string m_description;
    public MorseValue m_morsValue;

}

public enum MorseKey :int  {  Short=1, Long=2 , Space = 0 }
