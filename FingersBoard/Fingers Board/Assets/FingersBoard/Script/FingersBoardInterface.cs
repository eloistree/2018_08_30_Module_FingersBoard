﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class FingersBoardInterface: MonoBehaviour {
    
    public FingerDownUnityEvent onFingerDown;
    public FingerPressingUnityEvent onFingerPressing;
    public FingerMorseUnityEvent onFingerMorseDetected;
    public FingerMorseKeyUnityEvent onFingerMorseKeyDetected;
    
    public void Awake()
    {
        FingersBoard.onFingerDown += delegate (FingerType f, bool k) { onFingerDown.Invoke(f, k); };
        FingersBoard.onFingerPressing += delegate (FingerType f, bool k) { onFingerPressing.Invoke(f, k); };
        FingersBoard.onFingerMorseDetected += delegate (FingerType f, MorseValue k) { onFingerMorseDetected.Invoke(f, k); };
        FingersBoard.onFingerMorseKeyDetected += delegate (FingerType f, MorseKey k) { onFingerMorseKeyDetected.Invoke(f, k); };
    }
}

[System.Serializable]
public class FingersBoard
{

    private static FingersBoard m_instance;
    public static FingersBoard Instance {
        get {
            if (m_instance == null)
                m_instance = new FingersBoard();
            return m_instance;
        }
    }
    public FingersBoard() {
        if (m_instance == null)
            m_instance = this;

        foreach (FingerType fing in GetFingerTypeList())
        {
            Finger finger = GetFinger(fing);
            finger.m_onFingerDown += delegate (FingerType f, bool k) {
                if(onFingerDown!=null) onFingerDown(f, k); };
            finger.m_onFingerPressing += delegate (FingerType f, bool k) {
                if (onFingerPressing != null) onFingerPressing(f, k); };
            finger.m_onFingerMorseDetected += delegate (FingerType f, MorseValue k) {
                if (onFingerMorseDetected != null) onFingerMorseDetected(f, k); };
            finger.m_onFingerMorseKeyDetected += delegate (FingerType f, MorseKey k) {
                if (onFingerMorseKeyDetected != null) onFingerMorseKeyDetected(f, k); };
        }

    }

    public static FingerDownEvent onFingerDown;
    public static FingerPressingEvent onFingerPressing;
    public static FingerMorseEvent onFingerMorseDetected;
    public static FingerMorseKeyEvent onFingerMorseKeyDetected;



    public Hand m_left = new Hand(HandType.Left);
    public Hand m_right = new Hand(HandType.Right);

    public Finger GetFinger(FingerType finger) {
        switch (finger)
        {
            case FingerType.LeftLittle: return m_left.m_little;
            case FingerType.LeftRing: return m_left.m_ring;
            case FingerType.LeftMiddle: return m_left.m_middle;
            case FingerType.LeftIndex: return m_left.m_index;
            case FingerType.LeftThumb: return m_left.m_thumb;
            case FingerType.RightThumb: return m_right.m_thumb;
            case FingerType.RightIndex: return m_right.m_index;
            case FingerType.RightMiddle: return m_right.m_middle;
            case FingerType.RightRing: return m_right.m_ring;
            default:
                return m_right.m_little;
        }
    }


    public static void Notify(FingerType finger, MorseKey key) { Instance.GetFinger(finger).Notify(key); }
    public static void Notify(FingerType finger, MorseValue key) { Instance.GetFinger(finger).Notify(key); }
    public static void NotifyPressing(FingerType finger, bool isPressing) { Instance.GetFinger(finger).NotifyPressing(isPressing); }
    public static void NotifyDown(FingerType finger, bool isDown){ Instance.GetFinger(finger).NotifyDown(isDown); }





    FingerType[] GetFingerTypeList() { return GetEnumArray<FingerType>() ; }

    public static T[] GetEnumArray<T>() where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).OfType<T>().ToArray();
    }
    public static List<T> GetEnumList<T>() where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).OfType<T>().ToList();
    }
    public static List<string> GetEnumNamesList<T>() where T : struct, IConvertible
    {
        return Enum.GetNames(typeof(T)).ToList();
    }
}
[System.Serializable]
public class Hand
{
    public Hand(HandType name) {
        m_name = name;
        if (name== HandType.Left)
        {
            m_little =  new Finger(FingerType.LeftLittle);
            m_ring   =  new Finger(FingerType.LeftRing); ;
            m_middle =  new Finger(FingerType.LeftMiddle); 
            m_index  =  new Finger(FingerType.LeftIndex); 
            m_thumb  =  new Finger(FingerType.LeftThumb); 
        }
        else
        {
            m_little= new Finger(FingerType. RightLittle);
            m_ring  =  new Finger(FingerType.RightRing); ;
            m_middle=  new Finger(FingerType.RightMiddle); 
            m_index =  new Finger(FingerType.RightIndex); 
            m_thumb =  new Finger(FingerType.RightThumb); 
        }
    }

    public HandType m_name;
    public Finger m_little;
    public Finger m_ring;
    public Finger m_middle;
    public Finger m_index;
    public Finger m_thumb;


    private Finger[] m_fingers;
    public Finger[] GetFingers() {
        if (m_fingers == null) {
            m_fingers= new Finger[] { m_little, m_ring, m_middle, m_index, m_thumb };
        }
        return m_fingers;
    }
}

[System.Serializable]
public class Finger
{
    public Finger(FingerType name) {
        m_name = name;
    }
  


        public void Notify(MorseKey key)
    {
        if (m_onMorseKeyDetected != null)
            m_onMorseKeyDetected(key);
        if (m_onFingerMorseKeyDetected != null)
            m_onFingerMorseKeyDetected(m_name, key);
    }
    public void Notify(MorseValue key)
    {
        if (m_onMorseDetected != null)
            m_onMorseDetected(key);
        if (m_onFingerMorseDetected != null)
            m_onFingerMorseDetected(m_name,key);
    }
    public void NotifyPressing(bool isPressing) {

        if (m_isPressing != isPressing) {
            m_isPressing = isPressing;
            if (m_onFingerPressing != null)
                m_onFingerPressing(m_name, m_isPressing);
        }
    }
    public void NotifyDown(bool isDown) {
        if (m_isDown != isDown)
        {
            m_isDown = isDown;
            if (m_onFingerDown != null)
                m_onFingerDown(m_name, m_isDown);
        }
    }


    public bool m_isPressing;
    public bool m_isDown;

    public FingerType m_name;
    public MorseEvent m_onMorseDetected;
    public MorseKeyEvent m_onMorseKeyDetected;

    public FingerDownEvent m_onFingerDown;
    public FingerPressingEvent m_onFingerPressing;
    public FingerMorseEvent m_onFingerMorseDetected;
    public FingerMorseKeyEvent m_onFingerMorseKeyDetected;


}


//MORSE EVENT
[System.Serializable]
public class MorseUnityEvent : UnityEvent<MorseValue> { }
[System.Serializable]
public class MorseKeyUnityEvent : UnityEvent<MorseKey> { }

    public delegate void MorseEvent(MorseValue value);
    public delegate void MorseKeyEvent(MorseKey value);


//MORSE LINKED TO FINGER EVENT
[System.Serializable]
public class FingerMorseUnityEvent : UnityEvent<FingerType, MorseValue> { }
[System.Serializable]
public class FingerMorseKeyUnityEvent : UnityEvent<FingerType, MorseKey> { }

public delegate void FingerMorseEvent(FingerType finger, MorseValue value);
public delegate void FingerMorseKeyEvent(FingerType finger , MorseKey value);

// PRESSING
public delegate void FingerPressingEvent(FingerType finger, bool isPressing);
public delegate void FingerDownEvent(FingerType finger, bool isDown);

[System.Serializable]
public class FingerPressingUnityEvent : UnityEvent<FingerType, bool> { }
[System.Serializable]
public class FingerDownUnityEvent : UnityEvent<FingerType, bool> { }


public enum HandType : int { Left, Right}
public enum FingerType:int { LeftLittle=0, LeftRing = 1, LeftMiddle = 2, LeftIndex = 3, LeftThumb = 4, RightThumb = 5, RightIndex = 6, RightMiddle = 7, RightRing = 8, RightLittle = 9 }