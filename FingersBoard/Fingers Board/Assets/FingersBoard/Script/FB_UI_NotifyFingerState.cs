﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FB_UI_NotifyFingerState : MonoBehaviour {

    public FingerType m_finger;
    public FB_MorseDetection m_morseDetection;
    
	void Awake ()
    {
        m_morseDetection.onMorseKeyDetected.AddListener(OnKeyDetected);
        m_morseDetection.onMorsePressingState.AddListener(OnPressingChange);
        m_morseDetection.onMorseValueDetected.AddListener(OnMorseDetected);
    }

    private void OnMorseDetected(MorseValue arg0)
    {
        FingersBoard.Notify(m_finger, arg0);
    }

    private void OnKeyDetected(MorseKey arg0)
    {
        FingersBoard.Notify(m_finger, arg0);
    }

    private void OnPressingChange(bool arg0)
    {
        FingersBoard.NotifyPressing(m_finger, arg0);
    }

    void Update () {
 //       Fingersbor


    }
}
