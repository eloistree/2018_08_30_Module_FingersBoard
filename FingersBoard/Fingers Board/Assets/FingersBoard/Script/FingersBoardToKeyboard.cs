﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class FingersBoardToKeyboard : KeyMapMono {
    
    public FingerToLetters[] m_fingers;
    public UnityEvent m_onValueChanged;

    [System.Serializable]
    public class FingerToLetters
    {
        public string m_name;
        public FingerType m_finger;
        public List<FingerMorseToLetter> m_possibilities = new List<FingerMorseToLetter>();
    
        public FingerToLetters(FingerType finger)
        {
            m_name = finger.ToString();
            this.m_finger = finger;
        }
    }
    
    internal MorsePlus[] GetAllMorsePlus()
    {
        throw new NotImplementedException();
    }

    internal void GetAllMorseValue()
    {
        throw new NotImplementedException();
    }

    [System.Serializable]
    public class FingerMorseToLetter {
        public string m_description;
        public MorsePlus m_morse;
        public string m_text="";
    }


    private void Reset()
    {
        CheckForAllFingerToBe();
        InitialWithAtLeastBasicMorse();
        
    }

    private void InitialWithAtLeastBasicMorse()
    {
        for (int i = 0; i < 10; i++)
        {
            NewMethod(i, MorseKey.Short);
            NewMethod(i, MorseKey.Short, MorseKey.Short);
            NewMethod(i, MorseKey.Short, MorseKey.Short, MorseKey.Short);
            NewMethod(i, MorseKey.Long);
            NewMethod(i, MorseKey.Long, MorseKey.Long);
            NewMethod(i, MorseKey.Long, MorseKey.Long, MorseKey.Long);
        }
    }

    private void NewMethod(int i, params MorseKey[] keys)
    {
        m_fingers[i].m_possibilities.Add(new FingerMorseToLetter()
        {
            m_morse = new MorsePlus(keys)
        });
    }

    private void CheckForAllFingerToBe()
    {
        m_fingers = new FingerToLetters[10];
        for (int i = 0; i < 10; i++)
        {
            m_fingers[i] = new FingerToLetters((FingerType)i);
        }
    }

    public void OnValidate()
    {
        foreach (FingerToLetters fl in m_fingers)
        {
            foreach (FingerMorseToLetter   fml in fl.m_possibilities)
            {
                if (string.IsNullOrEmpty(fml.m_morse.m_description)) {
                 fml.m_morse.m_description = fml.m_morse.m_morsValue.ToString();
                }
                fml.m_description = fml.m_morse.m_description+(string.IsNullOrEmpty(fml.m_text)?" ":" ("+fml.m_text+")");
            }
        }
        m_onValueChanged.Invoke();
    }





    public override void NotifyMorse(FingerType finger, MorseValue value)
    {
        FingerMorseToLetter toDo;
        if (FingerHaveMorseValue(finger, value, out toDo)) {
            Debug.Log("ACTION: " + toDo.m_text);
            m_onActionValidated.Invoke(toDo.m_text);
        }
    }

    private bool FingerHaveMorseValue(FingerType finger, MorseValue value, out FingerMorseToLetter result)
    {
        result = null;
        FingerToLetters fingerLetters = GetFinger(finger);
        result = GetPossibily(fingerLetters, value);
        return result!=null;
    }

    private FingerMorseToLetter GetPossibily(FingerToLetters fingerLetters, MorseValue value)
    {
        FingerMorseToLetter fm;
        for (int i = 0; i < fingerLetters.m_possibilities.Count; i++)
        {
            fm = fingerLetters.m_possibilities[i];
            if (fm.m_morse.m_morsValue == value) {
                return fm;
            }
        }
        return null;
    }

    private FingerToLetters GetFinger(FingerType finger)
    {
        return m_fingers.Where((k) => k.m_finger == finger).First();
    }
    
    public override void NotifyMorse(FingerType finger, MorseKey value)
    {
    }

    public ActionValidated m_onActionValidated;
    [Serializable]
    public class ActionValidated : UnityEvent<string> { };
}
