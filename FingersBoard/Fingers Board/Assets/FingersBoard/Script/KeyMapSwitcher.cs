﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyMapSwitcher : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SwitchFor(string keymapName) {

    }
}

public interface IKeyMap {

    void SetName(string name);
    string GetName();
    void NotifyMorse(FingerType finger, MorseValue value);
    void NotifyMorse(FingerType finger, MorseKey value);
}

public class KeyMapManager {
    private static List<IKeyMap> m_keyMaps = new List<IKeyMap>();

    public static void RegisterKeyMap(IKeyMap keymap)
    {
        if (!m_keyMaps.Contains(keymap))
            m_keyMaps.Add(keymap);

    }
    public static void RemoveKeyMap(IKeyMap keymap)
    {
        m_keyMaps.Remove(keymap);

    }

    public static IKeyMap GetKeymapByName(string name)
    {
        for (int i = 0; i < m_keyMaps.Count; i++)
        {
            if (m_keyMaps[i].GetName() == name)
                return m_keyMaps[i];

        }
        return null;
    }
}

public abstract class KeyMapMono :MonoBehaviour, IKeyMap
{

  

    private string m_keymapName;
    public string GetName()
    {
        return m_keymapName;
    }
    public void SetName(string name)
    {
        if (string.IsNullOrEmpty(name))
            throw new Exception("Name Can be null or empty");
        m_keymapName = name;
    }

    public abstract void NotifyMorse(FingerType finger, MorseValue value);
    public abstract void NotifyMorse(FingerType finger, MorseKey value);


    private void Awake()
    {
        KeyMapManager.RegisterKeyMap(this);
    }
}
