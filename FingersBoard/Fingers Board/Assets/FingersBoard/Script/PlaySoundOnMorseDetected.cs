﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnMorseDetected : MonoBehaviour {

    public AudioSource m_source;
    public AudioClip [] m_shortClips;
    public AudioClip [] m_longClips;
    public AudioClip m_morseValue;
    public AudioClip m_fingerPressing;
    public AudioClip m_fingerRelease;

    public void PlayPressingState(bool isPressing) {
        if (isPressing)
            PlayAudioClip(m_fingerPressing);
        else PlayAudioClip(m_fingerRelease);
    }

    public void PlayMorseValue(MorseValue value ) {
        PlayAudioClip(m_morseValue);
    }
    public void PlayMorseKey(MorseKey key ) {
        if (key == MorseKey.Short)
            PlayShort();
        else PlayLong();
    }
    public void PlayShort() { PlayAudioClip(m_shortClips[Random.Range(0, m_shortClips.Length)]); }
    public void PlayLong() { PlayAudioClip(m_longClips[Random.Range(0, m_longClips.Length)]); }

    public void PlayAudioClip(AudioClip clip ) {
        if (clip != null) {

        m_source.Stop();
        m_source.clip = clip;
        m_source.Play();
        }
    }
}
