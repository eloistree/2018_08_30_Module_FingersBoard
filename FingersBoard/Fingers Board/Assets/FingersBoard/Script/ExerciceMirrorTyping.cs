﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExerciceMirrorTyping : MonoBehaviour {

    public string m_allowCharacter = "abcdefghijklmnopqrstuvwxyz";
    public InputField m_exerciceText;
    public InputField m_typedText;
    public Color m_fail = Color.red, m_success= Color.green;
    public int m_count=100;

    void Start ()
    {
        m_exerciceText.onValueChanged.AddListener(Refresh);
        m_typedText.onValueChanged.AddListener(Refresh);
        if (m_exerciceText.text.Length <= 0)
        {
            for (int i = 0; i < m_count; i++)
            {
                m_exerciceText.text +=
                    m_allowCharacter[UnityEngine.Random.Range(0, m_allowCharacter.Length - 1)] + " ";

            }
        }
    }

    private void Refresh(string text)
    {
        bool isSame = m_exerciceText.text.StartsWith( m_typedText.text);
        Text[] t = m_typedText.GetComponentsInChildren<Text>();
        foreach (Text st in t)
        {
            st.color = isSame ? m_success : m_fail;
        }

    }

    public void Apprehend(string text) {
        if (text == "DELETE") {
            if (m_typedText.text.Length > 0)
            m_typedText.text = m_typedText.text.Substring(0, m_typedText.text.Length - 1);
            return;
        }




        Debug.Log("V:" + text);
        int typeLength;
        int exerciceLength = m_exerciceText.text.Length;

        m_typedText.text += text;
        typeLength = m_typedText.text.Length;
        if (m_exerciceText.text.IndexOf(m_typedText.text)==0) {

            string s = m_exerciceText.text.Substring(typeLength, exerciceLength - typeLength);
            Debug.Log("Empty " + text + " " + typeLength);
            Debug.Log("D " + s );
            m_typedText.text ="";
            m_exerciceText.text = s;
        }
        
    }
    

}
