﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigFingersBoard : MonoBehaviour {


    public InputField m_commitDelay;
    public InputField m_longDelay;
    public InputField m_becomePressing;

    public void Start()
    {
        
        SetCommitDelay(float.Parse(m_commitDelay.text));
        SetLongDelay(float.Parse(m_longDelay.text));
        SetBecomePressing(float.Parse(m_becomePressing.text));

    }
    public void SetCommitDelay(float value)
    {
        foreach (FB_MorseDetection det in GetDetections())
        {
            det.m_blankCommitDelay = value;
        }

    }
    public void SetLongDelay(float value)
    {
        foreach (FB_MorseDetection det in GetDetections())
        {
            det.m_longDelay = value;
        }

    }
    public void SetBecomePressing(float value)
    {
        foreach (FB_MorseDetection det in GetDetections())
        {
            det.m_longBecomePressing = value;
        }

    }

    public FB_MorseDetection[] GetDetections() {

        return FindObjectsOfType<FB_MorseDetection>();
    }
}
