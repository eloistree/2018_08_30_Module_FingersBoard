﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ListenToFingerMorseCode : MonoBehaviour {


    public FingerMorseToActions[] m_actions;

    [System.Serializable]
    public class FingerMorseToActions {

        public FingerType m_fingerListened;
        public MorseValue m_morseEmitted;
        public UnityEvent m_toDoWhenNotify;
    }

	// Use this for initialization
	void Start () {
        for (int i = 0; i < m_actions.Length; i++)
        {
            FingersBoard.Instance.GetFinger(m_actions[i].m_fingerListened)
                .m_onFingerMorseDetected += MorseDetected;
        }

	}

    void MorseDetected(FingerType finger, MorseValue morse) {

        Debug.Log(string.Format("{0} detecte {1}: Do the think !", finger, morse.ToString()));
    }
	
}
